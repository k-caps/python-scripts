#!/home/kobi/Dev/git/python/venvs/autoreport/bin/python3

## Assumes a VPN connection is open. Will assume a 5 second delay to open a browser to the CRM.

import time
import file.py
from csv import reader

CUSTOMER = 'IDF (Bynet)'
ENTITLEMENT = 'מסגרת שעות קובי'
DEFAULT_DESCRIPTION = 'Ansible Postgres Rundeck Support'
DEFAULT_START_TIME = '7:00'
DEFAULT_END_TIME = '16:00'
ELEMENTS = elements.py.all_elements

#### TODO: Add check at program init for what screen size is being used, can have a different coordinate set based on size.

filename = 'hours.csv'
with open(filename, 'r') as csvfile:
     datareader = reader(csvfile)
     header = next(datareader)
     if header != None:
         for row in datareader:
            date = row[0]
            title = f'WD {date}'
            start_time = row[1] or DEFAULT_START_TIME
            end_time = row[2] or DEFAULT_END_TIME
            description = row[3] or DEFAULT_DESCRIPTION
            
            print(ELEMENTS.btn_new.x)
            add_case()

def add_case(date, title, start_time, end_time, description):
    # wait for a browser window to be open
    time.sleep(5)
    pyautogui.click(ELEMENTS.btn_new.x,ELEMENTS.btn_new.y)

def main():
    pass

if __name__ == "__main__":
    main()

