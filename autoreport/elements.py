#!/home/kobi/Dev/git/python/venvs/autoreport/bin/python3


#### TODO: add screen_mode variables, and make a function that returns an `all_elements` based on the screen mode
#'''
#switch: 
#    case screen_mode == "single_laptop":
#        all_elements = single_laptop_elements
#    case screen_mode == "laptop_home_screen:
#        all_elemnts = laptop_home_screen_elements
#
#etcetra
#'''
# Buttons, text fields, anything the mouse or keyboard interacts with, is an element

# For dual screen, laptop + home second screen
all_elements = {
    "btn_new":
        "x": 3040
        "y": 140    
    "btn_case":
        "x": 1624
        "y": 223
    "fld_customer":
        "x": 1610
        "y": 260
    "rec_customer":
        "x": 1655
        "y": 325
    "fld_case_title":
        "x": 1610
        "y": 310
    "fld_start_date":
        "x": 1620
        "y": 400
    "fld_start_time":
        "x": 1910
        "y": 400
    "fld_entitlement":
        "x": 2255
        "y": 310
    "rec_entitlement":
        "x": 2265
        "y": 365
    "fld_end_date":
        "x": 2245
        "y": 400
    "fld_end_time":
        "x": 2465
        "y": 400
    "fld_description":
        "x": 2715
        "y": 260
    "btn_save":
        "x": 3075
        "y": 560

#class elements:
#	def __init__(self, name, x, y):
#		self.name = name
#		self.x = x
#		self.y = y
#
#e = elements
#all_elements = [
#    e.('btn_new',3040,140),
#    e.('btn_case',1624,223),
#    e.('fld_customer',1610,260),
#    e.('rec_customer',1655,325),
#    e.('fld_case_title',1610,310),
#    e.('fld_start_date',1620,400),
#    e.('fld_start_time',1910,400),
#    e.('fld_entitlement',2255,310),
#    e.('rec_entitlement',2265,365),
#    e.('fld_end_date',2245,400),
#    e.('fld_end_time',2465,400),
#    e.('fld_description',2715,260),
#    e.('btn_save',3075,560)
#]
# 
#class elements:
#    def __init__(self, name, **kwargs):
#            self.name = name
#            for key,value in kwargs.items():
#                    print("X coord = {0},  y coord= {1}".format(key,value))
#
#elem  =  elements('new_case',x="1",y="2")
## output
#X coord = x,  y coord= 1
#X coord = y,  y coord= 2
