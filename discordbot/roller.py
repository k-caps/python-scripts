# bot.py
import os
import re
import discord
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if re.match("^\.r|.R",message.content):
         response = "Test response"
         await message.channel.send(response)

client.run(TOKEN)

