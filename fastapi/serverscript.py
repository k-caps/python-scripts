
#########################
# Install dependencies: #
########################

python3 -m venv fastapi
source fastapi/bin/activate
python3 -m  pip install  --upgrade pip
python3 -m  pip install  "fastapi[all]"


#######################
# Actual python code: #
#######################

#serverscript.py:
#!/usr/bin/python3
from fastapi import FastAPI

app = FastAPI()

@app.get("/state")
def get_local_state():
	return 'primary'

#########################
# To launch the server: #
#########################

# uvicorn serverscript:app --reload

# This will launch serverscript.py in the working directory




