import random
import time
import shutil

# Vars
natural_notes = ['A','B','C','D','E','F','G']
sharp_notes = ['A#','C#','D#','F#','G#']
flat_notes = ['Ab','Bb','Db','Eb','Gb']
seventh_notes = ['A7','B7','C7','D7','E7','F7','G7']
all_notes = natural_notes + sharp_notes + flat_notes + seventh_notes
end_time = time.time() + 60 * 15
columns = shutil.get_terminal_size().columns

# Logic
while time.time() < end_time:
    print(random.choice(all_notes).center(columns))
    time.sleep(1)
