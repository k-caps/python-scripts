import random
import time
import shutil
import tkinter as tk

# Vars
natural_notes = ['A','B','C','D','E','F','G']
sharp_notes = ['A#','C#','D#','F#','G#']
flat_notes = ['Ab','Bb','Db','Eb','Gb']
seventh_notes = ['A7','B7','C7','D7','E7','F7','G7']
all_notes = natural_notes + sharp_notes + flat_notes + seventh_notes
end_time = time.time() + 60 #* 15
columns = shutil.get_terminal_size().columns

# GUI
window = tk.Tk()

# Logic
while time.time() < end_time:
	note_box = tk.Label(text=random.choice(all_notes).center(columns))
	window.mainloop()
	note_box.pack()
	time.sleep(3)
	window.destroy()



