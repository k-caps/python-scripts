from PyQt5.QtWidgets import *
app = QApplication([])
app.setStyleSheet("QPushButton { margin: 3ex; }")
window = QWidget()
layout = QVBoxLayout()

# Set labels
countDownTimer = QLabel('')
layout.addWidget(countDownTimer)

# set buttons
selfDestructBtn = QPushButton('Click here to destroy the computer')
layout.addWidget(selfDestructBtn)

cancelSdBtn = QPushButton('You may click here to cancel the destruct sequence')
layout.addWidget(cancelSdBtn)

# set button click functions
def selfDestructClicked():
    sdWarning = QMessageBox()
    sdWarning.setText('Initiating self destruct timer.')
    sdWarning.exec_()

def cancelSdClicked():
    cancelSdWarning = QMessageBox()
    cancelSdWarning.setText('Cancelling self destruct sequence.')
    cancelSdWarning.exec_()

selfDestructBtn.clicked.connect(selfDestructClicked)
cancelSdBtn.clicked.connect(cancelSdClicked)

# Launch GUI
window.setLayout(layout)
window.show()

app.exec_()



