from PyQt5.QtWidgets import *
app = QApplication([])
app.setStyleSheet("QPushButton { margin: 3ex; }")
window = QWidget()
layout = QVBoxLayout()


# set buttons
selfDestruct = QPushButton('Click here to destroy the computer')

# set button click functions
def selfDestructClicked():
    sdWarning = QMessageBox()
    sdWarning.setText('Initiating self destruct timer.')
    sdWarning.exec_()

selfDestruct.clicked.connect(selfDestructClicked)


#selfDestruct.show()

app.exec_()
